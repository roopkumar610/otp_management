package com.hackathon.otp_management.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import com.hackathon.otp_management.validation.OTPIdExisting;
import com.hackathon.otp_management.validation.UserIdExisting;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * OTPGenerationRequestDTO act a DTO for holding/mapping request body for resend OTP.
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OTPResendRequestDTO {

	@NotNull(message="{NotNull.userId}")
	@Min(value = 1, message = "{Min.userId}")
	@UserIdExisting
	private Integer userId;
	
	@NotNull(message="{NotNull.otpId}")
	@OTPIdExisting
	private Integer otpId;
}
