package com.hackathon.otp_management.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.hackathon.otp_management.validation.OTPTypeExisting;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * OTPGenerationRequestDTO act a DTO for holding/mapping request body for generating OTP.
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OTPGenerationRequestDTO {
	
	@NotNull(message="{NotNull.userId}")
	@Min(value = 1, message = "{Min.userId}")
	private Integer userId;
	
	@NotNull(message="{NotNull.otpType}")
	@NotBlank(message = "{NotBlank.otpType}")
	@OTPTypeExisting
	private String otpType;

}
