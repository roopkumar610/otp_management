package com.hackathon.otp_management.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * OTPResponseDTO used to represent the DTO for otp responses.
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OTPResponseDTO {
	
	private Integer statusCode;
	
	private String message;

}
