package com.hackathon.otp_management.services;

import com.hackathon.otp_management.dto.OTPGenerationRequestDTO;
import com.hackathon.otp_management.dto.OTPResendRequestDTO;
import com.hackathon.otp_management.dto.OTPResponseDTO;

/**
 * UserService used to achieve abstraction between controller and service layer of User entity.
 * @author Roop Kumar R on 03/04/2020
 *
 */
public interface UserService {

	OTPResponseDTO generateOTP(OTPGenerationRequestDTO otpGenerationRequestDTO);

	OTPResponseDTO resendOTP(OTPResendRequestDTO otpResendRequestDTO);

}
