package com.hackathon.otp_management.services.impl;

import java.util.Calendar;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hackathon.otp_management.dto.OTPGenerationRequestDTO;
import com.hackathon.otp_management.dto.OTPResendRequestDTO;
import com.hackathon.otp_management.dto.OTPResponseDTO;
import com.hackathon.otp_management.entities.User;
import com.hackathon.otp_management.entities.UserOTP;
import com.hackathon.otp_management.exception.OTPNotFoundException;
import com.hackathon.otp_management.exception.ResendNotEnabledException;
import com.hackathon.otp_management.exception.UserNotFoundException;
import com.hackathon.otp_management.repositories.OTPTypeRepository;
import com.hackathon.otp_management.repositories.UserOTPRepository;
import com.hackathon.otp_management.repositories.UserRepository;
import com.hackathon.otp_management.services.UserService;
import com.hackathon.otp_management.util.AES;
import com.hackathon.otp_management.util.GenerateOTP;

import lombok.extern.slf4j.Slf4j;

/**
 * UserServiceImpl used to implement the business logic for the abstraction
 * provided by UserController component.
 * 
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserOTPRepository userOTPRepository;

	@Autowired
	OTPTypeRepository otpTypeRepository;

	@Value("#{T(java.lang.Integer).parseInt('${successCode}')}")
	Integer successCode;

	@Value("${otpGenerationSuccessMessage}")
	String otpGenerationSuccessMessage;

	@Value("#{T(java.lang.Integer).parseInt('${walletOTPLength}')}")
	Integer walletOTPLength;

	@Value("#{T(java.lang.Integer).parseInt('${passwordOTPLength}')}")
	Integer passwordOTPLength;

	@Value("$(secretKey)")
	String secretKey;

	@Value("${otpResendSuccessMessage}")
	String otpResendSuccessMessage;

	/**
	 * Used to generate OTP
	 * 
	 * @author Roop Kumar R on 03/04/2020
	 * 
	 * @param otpGenerationRequestDTO
	 * @throws UserNotFoundException
	 * @return OTPResponseDTO
	 */
	@Transactional
	@Override
	public OTPResponseDTO generateOTP(OTPGenerationRequestDTO otpGenerationRequestDTO) {

		String otp = null;
		if (otpGenerationRequestDTO.getOtpType().equalsIgnoreCase("wallet")) {
			otp = GenerateOTP.generateOTP(walletOTPLength, otpGenerationRequestDTO.getOtpType());
		} else {
			otp = GenerateOTP.generateOTP(passwordOTPLength, otpGenerationRequestDTO.getOtpType());
		}

		Optional<User> user = userRepository.findById(otpGenerationRequestDTO.getUserId());
		if (!user.isPresent()) {
			log.info("No user associated with " + otpGenerationRequestDTO.getUserId() + " id.");
			throw new UserNotFoundException("There is no user with id : " + otpGenerationRequestDTO.getUserId());
		}
		
		String encryptedOTP = AES.encrypt(otp, secretKey);
		UserOTP userOTP = new UserOTP();
		userOTP.setCreatedOn(Calendar.getInstance());
		userOTP.setOtp(encryptedOTP);
		userOTP.setOtpType(
				otpTypeRepository.findByOtpTypeNameContainingIgnoreCase(otpGenerationRequestDTO.getOtpType()).get(0));
		userOTP.setUser(user.get());

		userOTPRepository.save(userOTP);
		return new OTPResponseDTO(successCode, otpGenerationSuccessMessage);
	}

	@Override
	public OTPResponseDTO resendOTP(OTPResendRequestDTO otpResendRequestDTO) {
		
		Optional<User> user = userRepository.findById(otpResendRequestDTO.getUserId());
		if (!user.isPresent()) {
			log.info("No user associated with " + otpResendRequestDTO.getUserId() + " id.");
			throw new UserNotFoundException("There is no user with id : " + otpResendRequestDTO.getUserId());
		}
		Optional<UserOTP> userOTP = userOTPRepository.findById(otpResendRequestDTO.getOtpId());
		if (!userOTP.isPresent()) {
			log.info("OTP is not associated with " + otpResendRequestDTO.getUserId() + " id.");
			throw new OTPNotFoundException("There is no otp associated with id : " + otpResendRequestDTO.getUserId());
		}
		
		log.info("************** OTP created time in seconds :" + userOTP.get().getCreatedOn().getTimeInMillis()/1000);
		log.info("************** Current system time in seconds :" + System.currentTimeMillis()/1000);
		long seconds = (System.currentTimeMillis()-userOTP.get().getCreatedOn().getTimeInMillis())/1000;
		
		if(seconds < 15) {
			log.info("Resend is not enabled.");
			throw new ResendNotEnabledException("Resend is not enabled.");
		} else if (seconds > 60) {
			log.info("OTP expired");
			throw new OTPNotFoundException("OTP expired");
		}
		
		return new OTPResponseDTO(successCode, otpResendSuccessMessage);
	}

}
