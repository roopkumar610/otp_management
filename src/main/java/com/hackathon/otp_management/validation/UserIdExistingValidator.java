package com.hackathon.otp_management.validation;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hackathon.otp_management.repositories.UserRepository;

/**
 * A validator class to check whether the given user ID is existing or not.
 * 
 * @author Roop Kumar R on 31/03/2020
 *
 */
@Component
public class UserIdExistingValidator implements ConstraintValidator<UserIdExisting, Integer> {

	@Autowired
	private UserRepository userRepository;

	@Override
	public boolean isValid(Integer userId, ConstraintValidatorContext context) {
		return Objects.isNull(userId) || userRepository.findById(userId).isPresent();
	}

}
