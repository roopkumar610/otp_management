package com.hackathon.otp_management.validation;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hackathon.otp_management.repositories.UserOTPRepository;


/**
 * A validator class to check whether the given OTP ID is existing or not.
 * 
 * @author Roop Kumar R on 31/03/2020
 *
 */
@Component
public class OTPIdExistingValidator implements ConstraintValidator<OTPIdExisting, Integer> {

	@Autowired
	private UserOTPRepository userOTPRepository;

	@Override
	public boolean isValid(Integer otpId, ConstraintValidatorContext context) {
		return Objects.isNull(otpId) || userOTPRepository.findById(otpId).isPresent();
	}

}
