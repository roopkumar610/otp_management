package com.hackathon.otp_management.validation;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hackathon.otp_management.repositories.OTPTypeRepository;

/**
 * OTPTypeExistingValidator used to check the given otp type is existing or not.
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Component
public class OTPTypeExistingValidator implements ConstraintValidator<OTPTypeExisting, String>{

	@Autowired
	OTPTypeRepository otpTypeRepository;
	
	@Override
	public boolean isValid(String otpType, ConstraintValidatorContext context) {
		return Objects.isNull(otpType) || (otpTypeRepository.findByOtpTypeNameContainingIgnoreCase(otpType).size() > 0);
	}
}
