package com.hackathon.otp_management.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * A custom constraint annotation to check whether the given user id exists or not
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = UserIdExistingValidator.class)
public @interface UserIdExisting {
    String message() default "User is not registered.";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
