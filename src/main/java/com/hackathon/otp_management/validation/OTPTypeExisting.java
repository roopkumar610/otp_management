package com.hackathon.otp_management.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * A custom constraint annotation to check whether the given OTP Type is exists
 * or not
 * 
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD, ElementType.ANNOTATION_TYPE,
		ElementType.CONSTRUCTOR, ElementType.TYPE_USE })
@Constraint(validatedBy = OTPTypeExistingValidator.class)
public @interface OTPTypeExisting {
	
	Class<?>[] groups() default { };
	String message() default "{OTP Type is invalid.}";
	Class<? extends Payload> [] payload() default { };
	

}
