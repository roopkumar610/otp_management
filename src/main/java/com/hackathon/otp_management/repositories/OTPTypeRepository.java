package com.hackathon.otp_management.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hackathon.otp_management.entities.OTPType;

/**
 * OTPTypeRepository component is used to do CRUD, Paging and Sort operations on
 * OTP Type entity.
 * 
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Repository
public interface OTPTypeRepository extends JpaRepository<OTPType, Integer> {

	/**
	 * Used to fetch all the OTP Type details whose name contains otpType
	 * characters.
	 * 
	 * @param otpType - OTP Type name
	 * @return - List<OTPType>
	 */
	List<OTPType> findByOtpTypeNameContainingIgnoreCase(String otpType);

}
