package com.hackathon.otp_management.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hackathon.otp_management.entities.UserOTP;

/**
 * UserOTPRepository component is used to do CRUD, Paging and Sort operations on UserOTP entity.
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Repository
public interface UserOTPRepository extends JpaRepository<UserOTP, Integer> {

}
