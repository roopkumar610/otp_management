package com.hackathon.otp_management.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hackathon.otp_management.entities.User;

/**
 * UserRepository component is used to do CRUD, Paging and Sort operations on User entity.
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
