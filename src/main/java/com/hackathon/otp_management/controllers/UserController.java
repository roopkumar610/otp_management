package com.hackathon.otp_management.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.otp_management.dto.OTPGenerationRequestDTO;
import com.hackathon.otp_management.dto.OTPResendRequestDTO;
import com.hackathon.otp_management.dto.OTPResponseDTO;
import com.hackathon.otp_management.services.UserService;

/**
 * UserController component is used to handle the requests related to user.
 * @author Roop Kumar R on 03/04/2020
 *
 */
@RestController
@RequestMapping(value="/users")
public class UserController {
	
	@Autowired
	UserService userService;
	
	/**
	 * generateOTP method is used to generate OTP for Password reset and Payment wallet.
	 * 
	 * @author Roop Kumar R on 03/04/2020
	 * @param otpGenerationRequestDTO
	 * @return ResponseEntity<OTPResponseDTO>
	 */
	@PostMapping(value="/generate-otp")
	public ResponseEntity<OTPResponseDTO> generateOTP(@Valid @RequestBody OTPGenerationRequestDTO otpGenerationRequestDTO) {
		return new ResponseEntity<OTPResponseDTO>(userService.generateOTP(otpGenerationRequestDTO), HttpStatus.OK);
	}
	
	
	/**
	 * generateOTP method is used to resend OTP for Password reset and Payment wallet.
	 * 
	 * @author Roop Kumar R on 03/04/2020
	 * @param otpResendRequestDTO
	 * @return ResponseEntity<OTPResponseDTO>
	 */
	@PostMapping(value="/resend-otp")
	public ResponseEntity<OTPResponseDTO> resendOTP(@Valid @RequestBody OTPResendRequestDTO otpResendRequestDTO ) {
		return new ResponseEntity<OTPResponseDTO>(userService.resendOTP(otpResendRequestDTO), HttpStatus.OK);
	}

}
