package com.hackathon.otp_management.util;

import java.util.Random;

/**
 * GenerateOTP used to generate OTP.
 * 
 * @author Roop Kumar R on 03/04/2020
 *
 */
public class GenerateOTP {

	public static String generateOTP(int length, String otpType) {
		int leftLimit = 48;
		int rightLimit = 122;
		Random rand = new Random();

		if (otpType.equalsIgnoreCase("WALLET")) {
			int m = (int) Math.pow(10, length - 1);
			return String.valueOf(m + rand.nextInt(9 * m));
		} else {
			return rand.ints(leftLimit, rightLimit + 1).filter(i -> (i <= 57 || i > 65) && (i <= 90 || i >= 97))
					.limit(length).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
					.toString();
		}
	}
}
