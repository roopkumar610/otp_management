package com.hackathon.otp_management.exception;

import java.util.Date;
import java.util.List;

/**
 * A exception class for API's responses. Created by Roop Kumar R on 27/03/2020.
 */
public class ExceptionResponse {

	private Date timestamp;
	private String message;
	private List<String> details;

	public ExceptionResponse(Date timestamp, String message, List<String> details) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return message;
	}

	public List<String> getDetails() {
		return details;
	}

}
