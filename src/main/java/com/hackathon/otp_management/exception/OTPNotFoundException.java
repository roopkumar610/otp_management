package com.hackathon.otp_management.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A customized exception class to handle no otp records found scenario.
 * @author Roop Kumar R on 03/04/2020
 *
 */
@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.NOT_FOUND)
public class OTPNotFoundException extends RuntimeException {
	public OTPNotFoundException(String message) {
		super(message);
	}
}