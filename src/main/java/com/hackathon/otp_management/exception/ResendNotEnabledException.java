package com.hackathon.otp_management.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Roop Kumar R on 29/03/2020
 *
 */
@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResendNotEnabledException extends RuntimeException {
	public ResendNotEnabledException(String message) {
		super(message);
	}
}