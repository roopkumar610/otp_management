package com.hackathon.otp_management.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import javax.validation.ConstraintViolationException;

/**
 * A customizable class to define exception handling for all the exceptions.
 * 
 * @author Roop Kumar R on 03/04/2020
 *
 */
@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(CustomizedResponseEntityExceptionHandler.class);

	/**
	 * Used to handle all the exceptions
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		log.error(ex.getMessage(), ex);
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), "Please contact the administrator",
				Arrays.asList(request.getDescription(false)));
		return new ResponseEntity(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Used to handle no user record found exception
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ExceptionHandler(UserNotFoundException.class)
	public final ResponseEntity<Object> handleUserNotFoundExceptions(UserNotFoundException ex, WebRequest request) {
		log.error(ex.getMessage(), ex);
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
				Arrays.asList(request.getDescription(false)));
		return new ResponseEntity(exceptionResponse, HttpStatus.valueOf(601));
	}
	
	/**
	 * Used to handle no otp found exception
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ExceptionHandler(OTPNotFoundException.class)
	public final ResponseEntity<Object> handleOTPNotFoundExceptions(OTPNotFoundException ex, WebRequest request) {
		log.error(ex.getMessage(), ex);
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
				Arrays.asList(request.getDescription(false)));
		return new ResponseEntity(exceptionResponse, HttpStatus.valueOf(602));
	}
	
	/**
	 * Used to handle no otp found exception
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ExceptionHandler(OTPExpiredException.class)
	public final ResponseEntity<Object> handleOTPExpiredExceptions(OTPExpiredException ex, WebRequest request) {
		log.error(ex.getMessage(), ex);
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
				Arrays.asList(request.getDescription(false)));
		return new ResponseEntity(exceptionResponse, HttpStatus.valueOf(603));
	}
	
	/**
	 * Used to handle no otp found exception
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ExceptionHandler(ResendNotEnabledException.class)
	public final ResponseEntity<Object> handleResendNotEnabledExceptions(ResendNotEnabledException ex, WebRequest request) {
		log.error(ex.getMessage(), ex);
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
				Arrays.asList(request.getDescription(false)));
		return new ResponseEntity(exceptionResponse, HttpStatus.valueOf(604));
	}

	/**
	 * Used to stores the name and post-validation error message of each invalid
	 * field and sends it back to the client as a JSON representation
	 * 
	 * @param ex
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> details = new ArrayList<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			details.add(error.getDefaultMessage());
		}
		ExceptionResponse errorDetails = new ExceptionResponse(new Date(), "Validation Failed", details);
		return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Used to handle the constraint violation exceptions
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<Object> handleConstraintViolationException(Exception ex, WebRequest request) {
		log.error(ex.getMessage(), ex);

		ExceptionResponse errorDetails = new ExceptionResponse(new Date(), "Constraint Violation Failed",
				Arrays.asList(ex.getMessage()));
		return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
	}
}
