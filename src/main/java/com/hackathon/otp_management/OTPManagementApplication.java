package com.hackathon.otp_management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This class is used to bootstrap the OTP Management application.
 * @author Roop Kumar R on 03/04/2020
 *
 */
@SpringBootApplication
public class OTPManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(OTPManagementApplication.class, args);
	}

}
