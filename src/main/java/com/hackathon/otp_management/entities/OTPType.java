package com.hackathon.otp_management.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * OTPType class represents the otp_type table.
 * 
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "otp_type")
public class OTPType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "otp_type_id")
	private Integer otpTypeId;

	@NotNull(message = "{NotNull.otpTypeName}")
	@NotBlank(message = "{NotBlank.otpTypeName}")
	@Column(name = "otp_type_name", nullable = false, unique = true)
	private String otpTypeName;
}
