package com.hackathon.otp_management.entities;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * UserOTP represents the user_otp Table.
 * 
 * @author Roop Kumar R on 03/04/2020
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_otp")
public class UserOTP {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_otp_id")
	private Integer userOtpId;

	@NotNull(message = "{NotNull.createdOn}")
	@Column(name = "created_on", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar createdOn;

	@NotNull(message = "{NotNull.otp}")
	@NotBlank(message = "{NotBlank.otp}")
	@Column(name = "otp", nullable = false)
	private String otp;

	@ManyToOne(optional = true, cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "otp_type_id", referencedColumnName = "otp_type_id")
	private OTPType otpType;

	@ManyToOne(optional = true, cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User user;

}
